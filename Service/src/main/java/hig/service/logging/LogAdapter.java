package hig.service.logging;

import java.util.function.Supplier;
import org.springframework.stereotype.Component;


@Component
public interface LogAdapter {

    void log(Level level, Supplier<String> messageSupplier);
}
