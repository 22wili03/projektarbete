package hig.service.logging;


public enum Level {
    INFO, DEBUG, WARNING, ERROR;
}
