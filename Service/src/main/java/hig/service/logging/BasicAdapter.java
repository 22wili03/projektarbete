package hig.service.logging;

import java.util.function.Supplier;


public class BasicAdapter implements LogAdapter {

    @Override
    public void log(Level level, Supplier<String> messageSupplier) {
        System.out.println(messageSupplier.get());
    }
}
