package hig.service.room;

import hig.domain.Room;
import hig.repository.RoomRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RoomService {
    
    private final RoomRepository repository;
    private final RoomMapper mapper;

    @Autowired
    public RoomService(RoomRepository repository, RoomMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }
    
    public Room create(Room room) {
        return repository.save(room);
    }

    public List<RoomDTO> findAll() {
        return mapper.toDtoList(repository.findAll());
    }
    
    public void delete(Long id) {
        if(repository.findById(id).get().getCleaningSessions().size() > 0)
            throw new IllegalStateException("Cannot remove room with cleaning sessions");
        repository.deleteById(id);
    }
}
