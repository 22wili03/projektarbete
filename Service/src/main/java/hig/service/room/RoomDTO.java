package hig.service.room;


public record RoomDTO(Long id, String name) {
}
