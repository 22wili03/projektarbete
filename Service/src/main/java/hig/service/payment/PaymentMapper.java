package hig.service.payment;

import hig.domain.Payment;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;


@Component
@Mapper(componentModel = "spring")
public interface PaymentMapper {

    PaymentDTO toDto(Payment payment);
    
    List<PaymentDTO> toDtoList(List<Payment> payments);
}
