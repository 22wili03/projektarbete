package hig.service.payment;

import hig.domain.*;
import hig.repository.*;
import jakarta.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PaymentService {

    private final PaymentRepository repository;
    private final PaydayRepository paydayRepository;
    private final LatestPaydayRepository latestPaydayRepository;
    private final PersonRepository personRepository;
    private final RoomRepository roomRepository;
    private final PaymentMapper mapper;

    @Autowired
    public PaymentService(PaydayRepository paydayRepository, PaymentRepository repository, PersonRepository personRepository, RoomRepository roomRepository, LatestPaydayRepository latestPaydayRepository, PaymentMapper mapper) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.roomRepository = roomRepository;
        this.paydayRepository = paydayRepository;
        this.latestPaydayRepository = latestPaydayRepository;
        this.mapper = mapper;
    }
    
    // Create a new payment session and returns it
    public Payment create(String personalTag, LocalDateTime time, Double amount) {
        Person p = personRepository.findByTag(personalTag).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));
        Optional<LatestPayday> lp = latestPaydayRepository.findByPersonId(p.getId());
        if(lp.isPresent() && lp.get().getDateOfLatestPayday().compareTo(time) > 0)
            throw new IllegalArgumentException("Kan inte sätta payment med datum tidigare än senaste lönesättning");
        // Kolla om time är större än förra deadlinen
        // Om inte, kasta exception
        return repository.save(new Payment(p, time, amount));
    }
    
}
