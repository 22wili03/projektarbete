package hig.service.payment;

import hig.service.person.PersonSimpleDTO;
import java.time.LocalDateTime;


public record PaymentDTO(PersonSimpleDTO person, LocalDateTime time, Double amount) {}
