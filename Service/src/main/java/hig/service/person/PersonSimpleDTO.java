package hig.service.person;


public record PersonSimpleDTO(String name, Integer age) {
}
