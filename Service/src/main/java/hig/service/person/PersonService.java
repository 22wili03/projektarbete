package hig.service.person;

import hig.domain.Person;
import hig.repository.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PersonService {

    private final PersonRepository repository;
    private final PersonMapper mapper;

    @Autowired
    public PersonService(
            PersonRepository repository,
            PersonMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<PersonSimpleDTO> getAllPeople() {
        return mapper.toSimpleDtoList(repository.findAll());
    }

    public PersonSimpleDTO create(Person person) {
        
        return mapper.toSimpleDto(repository.save(person));
    }

    public PersonSimpleDTO getPerson(Long id) {
        return mapper.toSimpleDto(repository.findById(id).orElseThrow());
    }
    
    public void delete(Person person) {
        repository.delete(person);
    }
}
