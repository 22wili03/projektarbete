package hig.service.person;


public record PersonDTO(String firstName, String lastName) {
}
