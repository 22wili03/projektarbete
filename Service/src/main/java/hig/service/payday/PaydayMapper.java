package hig.service.payday;

import hig.domain.Payday;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;


@Component
@Mapper(componentModel = "spring")
public interface PaydayMapper {

    PaydayDTO toDto(Payday payday);
    
    List<PaydayDTO> toDtoList(List<Payday> paydays);
}
