package hig.service.payday;

import hig.domain.*;
import hig.repository.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PaydayService {
    private final PaydayMapper mapper;
    private PaydayRepository repository;
    private PersonRepository personRepository;
    private LatestPaydayRepository latestPaydayRepository;

    @Autowired
    public PaydayService(PaydayMapper mapper, PaydayRepository repository, PersonRepository personRepository, LatestPaydayRepository latestPaydayRepository) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.latestPaydayRepository = latestPaydayRepository;
        this.mapper = mapper;
    }
    
    // Create a new payday and sets the deadline for the next one
    public Payday create(List<Long> personIdList, LocalDateTime dateOfDeadline) {
        updateLatestPaydays(personIdList);
        LocalDateTime currentDateTime = LocalDateTime.now();
        if(currentDateTime.compareTo(dateOfDeadline) >= 0 || !repository.findByDateOfDeadlineGreaterThan(dateOfDeadline).isEmpty()){
            throw new IllegalArgumentException("Cant have a deadline earlier current date or any other deadline");
        }
        return repository.save(new Payday(currentDateTime, dateOfDeadline));
    }
    
    // Create a new payday and automatically sets the deadline to one month after the previous deadline
    public Payday create(List<Long> personIdList) {
        
        // The most recently used payday deadline
        LocalDateTime dateOfLastDeadline;
        
        // Sets last used deadline to current date if no previous deadline exists.
        if(repository.findTopByOrderByIdDesc().isEmpty())
            dateOfLastDeadline = LocalDateTime.now();
        else
            dateOfLastDeadline = repository.findTopByOrderByIdDesc().get().getDateOfDeadline();
        
        // Sets new deadline to previous deadline plus one month
        LocalDateTime dateOfDeadline = dateOfLastDeadline.plusMonths(1);
        
        return create(personIdList, dateOfDeadline);
    }
    
    public void updateLatestPaydays(List<Long> ids) {
        
        List<Person> persons = personRepository.findAllById(ids);
        List<Long> existingIds = persons.stream().map(Person::getId).collect(Collectors.toList());
        
        // Iterates the validated ids
        for (Long id : existingIds) {
            Optional<LatestPayday> OptionalLp = latestPaydayRepository.findByPersonId(id);
            // Updates if it already exists
            if(OptionalLp.isPresent()) {
                LatestPayday lp = OptionalLp.get();
                lp.setDateOfLatestPayday(LocalDateTime.now());
                latestPaydayRepository.save(lp); 
            } else {
                // Creates new if it doesnt exist
                Person person = personRepository.findById(id).get();
                LatestPayday newLp = new LatestPayday(person, LocalDateTime.now());
                latestPaydayRepository.save(newLp);
            }  
        }
    }
    
    public Payday update(LocalDateTime dateOfNewDeadline) {
        Payday lastPayday = repository.findTopByOrderByIdDesc().get();
        if(lastPayday.getDateOfDeadline().compareTo(dateOfNewDeadline) > 0){
            throw new IllegalArgumentException("Cant have a deadline earlier current date or any other deadline");
        }
        lastPayday.setDateOfDeadline(dateOfNewDeadline);
        return repository.save(lastPayday);
    }
    
}
