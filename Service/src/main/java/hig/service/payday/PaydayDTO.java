package hig.service.payday;

import java.time.LocalDateTime;


public record PaydayDTO(LocalDateTime dateStarts, LocalDateTime dateEnds) {}
