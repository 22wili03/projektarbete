package hig.service.cleaning;

import hig.domain.*;
import hig.repository.*;
import jakarta.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CleaningService {

    private final CleaningRepository repository;
    private final PersonRepository personRepository;
    private final RoomRepository roomRepository;
    private final CleaningMapper mapper;

    @Autowired
    public CleaningService(CleaningRepository repository, PersonRepository personRepository, RoomRepository roomRepository, CleaningMapper mapper) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.roomRepository = roomRepository;
        this.mapper = mapper;
    }
    
    // Returns ceaning sessions by roomId and after given date and time
    public List<CleaningDTO> getByRoomIdAndDateTime(Long roomId, LocalDateTime dateTime) {
        return mapper.toDtoList(repository.findByWhereIdAndWhenGreaterThan(roomId, dateTime));
    }
    
    public List<CleaningDTO> getByPersonIdAndDateTimeInterval(Long personId, LocalDateTime dateFrom, LocalDateTime dateTo) {
        return mapper.toDtoList(repository.findByWhoIdAndWhenGreaterThanAndWhenLessThan(personId, dateFrom, dateTo));
    }
    
    // Returns cleaning sessions by roomId for the current day
    public List<CleaningDTO> getByRoomIdForCurrentDay(Long roomId) {
        return mapper.toDtoList(repository.findByWhereIdAndWhenGreaterThan(roomId, LocalDate.now().atStartOfDay()));
    }
    
    // Create a new cleaning session and returns it
    public CleaningDTO create(Long roomId, String personalTag) {
        Person p = personRepository.findByTag(personalTag).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));
        Room r = roomRepository.findById(roomId).orElseThrow(() -> new EntityNotFoundException("No room with given id found"));
        return mapper.toDto(repository.save(new Cleaning(p, r)));
    }
    
    public long TagToId(String tag){
        if(personRepository.findByTag(tag).get() != null)
            return personRepository.findByTag(tag).get().getId();
        throw new EntityNotFoundException("No person with given key found");
    }
}
