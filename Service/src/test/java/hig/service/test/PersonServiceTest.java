package hig.service.test;

import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import hig.domain.Person;
import hig.repository.PersonRepository;
import hig.service.person.PersonMapper;
import hig.service.person.PersonService;
import hig.service.person.PersonSimpleDTO;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    @Mock
    private PersonMapper personMapper;

    @InjectMocks
    private PersonService personService;
    
    @Test
    public void testGetAllPeople() {
        // Mocking the repository to return a list of persons
        List<Person> persons = new ArrayList<>();
        when(personRepository.findAll()).thenReturn(persons);

        // Mocking the mapper
        when(personMapper.toSimpleDtoList(persons)).thenReturn(new ArrayList<>());

        // Call the method under test
        personService.getAllPeople();

        // Verify that the repository method was called
        verify(personRepository).findAll();

        // Verify that the mapper method was called
        verify(personMapper).toSimpleDtoList(persons);
    }

    @Test
    public void testCreate() {
        // Mocking the repository to return a person
        Person person = new Person();
        when(personRepository.save(person)).thenReturn(person);

        // Mocking the mapper
        when(personMapper.toSimpleDto(person)).thenReturn(new PersonSimpleDTO("Noob Lindahl", 20));

        // Call the method under test
        personService.create(person);

        // Verify that the repository method was called
        verify(personRepository).save(person);

        // Verify that the mapper method was called
        verify(personMapper).toSimpleDto(person);

    }

    @Test
    public void testGetPerson() {
        // Mocking the repository to return an optional person
        Person person = new Person();
        when(personRepository.findById(1L)).thenReturn(Optional.of(person));

        // Mocking the mapper
        when(personMapper.toSimpleDto(person)).thenReturn(new PersonSimpleDTO("Noob Lindahl", 20));

        // Call the method under test
        personService.getPerson(1L);

        // Verify that the repository method was called
        verify(personRepository).findById(1L);

        // Verify that the mapper method was called
        verify(personMapper).toSimpleDto(person);
    }

    @Test
    public void testDelete() {
        // Mocking the method under test
        Person person = new Person();
        personService.delete(person);

        // Verify that the repository method was called
        verify(personRepository).delete(person);
    }

}
