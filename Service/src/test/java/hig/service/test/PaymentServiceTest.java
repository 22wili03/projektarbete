package hig.service.test;

import hig.domain.LatestPayday;
import hig.domain.Payment;
import hig.domain.Person;
import hig.repository.LatestPaydayRepository;
import hig.repository.PaymentRepository;
import hig.repository.PersonRepository;
import hig.service.payment.PaymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDateTime;
import java.util.Optional;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

public class PaymentServiceTest {

    //private Person person;
    private LocalDateTime time;
    private Double amount;

    @Mock
    private PaymentRepository paymentRepository;

    @Mock
    private PersonRepository personRepository;
    
    @Mock
    private LatestPaydayRepository latestPaydayRepository;
    
    @Mock
    private LatestPayday latestPayday;
    
    @Mock
    private Person person;

    @InjectMocks
    private PaymentService paymentService;
    
    @BeforeEach
    public void setUp() {
        time = LocalDateTime.now();
        amount = 100.0;
        MockitoAnnotations.openMocks(this);
    }

    @Test
    // Test the Constructor
    public void testPaymentConstructor() {  
        Payment payment = new Payment(person, time, amount);
        assertEquals(person, payment.getPerson());
        assertEquals(time, payment.getTime());
        assertEquals(amount, payment.getAmount());
    }

    @Test
    // Test the getters and setters 
    public void testGetters() {
        Payment payment = new Payment();
        payment.setPerson(person);
        payment.setTime(time);
        payment.setAmount(amount);

        assertEquals(person, payment.getPerson());
        assertEquals(time, payment.getTime());
        assertEquals(amount, payment.getAmount());
    }

    @Test
    // Test Create method
    public void testCreatePayment() {  
        String personalTag = "testTag";
        person.setTag(personalTag);

        when(personRepository.findByTag(personalTag)).thenReturn(Optional.of(person));
        when(paymentRepository.save(any(Payment.class))).thenAnswer(i -> i.getArguments()[0]);
        when(latestPaydayRepository.findByPersonId(any(Long.class))).thenReturn(Optional.of(latestPayday));
        when(latestPayday.getDateOfLatestPayday()).thenReturn(LocalDateTime.now().minusDays(1));

        Payment payment = paymentService.create(personalTag, time, amount);

        assertNotNull(payment);
        assertEquals(person, payment.getPerson());
        assertEquals(time, payment.getTime());
        assertEquals(amount, payment.getAmount());

        verify(personRepository).findByTag(personalTag);
        verify(paymentRepository).save(any(Payment.class));
    } 
}
