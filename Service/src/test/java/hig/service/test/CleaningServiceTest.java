package hig.service.test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import hig.service.cleaning.CleaningDTO;
import hig.service.cleaning.CleaningMapper;
import hig.service.cleaning.CleaningService;
import hig.service.person.PersonSimpleDTO;
import hig.service.room.RoomDTO;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CleaningServiceTest {

    @Mock
    private CleaningRepository cleaningRepository;

    @Mock
    private PersonRepository personRepository;

    @Mock
    private RoomRepository roomRepository;

    @Mock
    private CleaningMapper cleaningMapper;

    @InjectMocks
    private CleaningService cleaningService;

    @BeforeEach
    public void setUp() {
    }

    @Test
    public void testCreate() {
        // Mock data
        Long roomId = 1L;
        String personalKey = "key";
        Person person = new Person();
        Room room = new Room("e252");
        Cleaning cleaning = new Cleaning(person, room);
        CleaningDTO cleaningDTO = new CleaningDTO(new RoomDTO(1L, "e252"), new PersonSimpleDTO("William Lindahl", 20), LocalDateTime.now()); // Assuming you have a CleaningDTO constructor

        when(personRepository.findByTag(personalKey)).thenReturn(Optional.of(person));
        when(roomRepository.findById(roomId)).thenReturn(Optional.of(room));
        when(cleaningRepository.save(any(Cleaning.class))).thenReturn(cleaning);
        when(cleaningMapper.toDto(cleaning)).thenReturn(cleaningDTO);

        // Call the method under test
        CleaningDTO result = cleaningService.create(roomId, personalKey);

        // Verify that methods were called
        verify(personRepository).findByTag(personalKey);
        verify(roomRepository).findById(roomId);
        verify(cleaningRepository).save(any(Cleaning.class));
        verify(cleaningMapper).toDto(cleaning);

        // Assert the result
        assertNotNull(result);
        
    }
}
