package hig.service.test;

import hig.domain.Cleaning;
import hig.domain.Person;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import hig.domain.Room;
import hig.service.room.RoomDTO;
import hig.repository.RoomRepository;
import hig.service.room.RoomService;
import hig.service.room.RoomMapper;
import java.util.Arrays;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RoomServiceTest {

    @Mock
    private RoomRepository roomRepository;

    @Mock
    private RoomMapper roomMapper;

    @InjectMocks
    private RoomService roomService;
    
    private Room room;  // Room entity for testing
    
    @Test
    public void testCreateRoom() {
        // Creating a room
        Room savedRoom = new Room("Test Room");
        when(roomRepository.save(room)).thenReturn(savedRoom);

        // Creating a new room
        Room createdRoom = roomService.create(room);
        
        // Comparing the two created rooms
        assertEquals(savedRoom, createdRoom);
    }
    
    @Test
    void testFindAll() {
        room = new Room("Test Room");
        
        // Mock repository.findAll to return a list containing the room object
        when(roomRepository.findAll()).thenReturn(Arrays.asList(room));
        
        // Mock mapper.toDtoList to return a list containing the same room object
        when(roomMapper.toDtoList(anyList())).thenAnswer(invocation -> invocation.getArgument(0));
        
        // Call the findAll method
        List<RoomDTO> roomList = roomService.findAll();
        
        // Verify that the returned list is not null and contains the correct room
        assertNotNull(roomList);
        assertEquals(1, roomList.size());
        assertEquals(room, roomList.get(0));
        
        // Verify that findAll and toDtoList methods were called once
        verify(roomRepository, times(1)).findAll();
        verify(roomMapper, times(1)).toDtoList(anyList());
    }
    
    @Test
    public void testDeleteRoomWithCleaningSessions() {
        // Given
        Long roomId = 111L;
        Room room = new Room("Test Room2");
        Person person = new Person();
        room.setId(roomId);
        
        Cleaning cleaningSession = new Cleaning(person, room);
        room.setCleaningSessions(List.of(cleaningSession)); // Assuming you have a setCleaningSessions method

        when(roomRepository.findById(roomId)).thenReturn(Optional.of(room));
        
        // When and Then
        assertThrows(IllegalStateException.class, () -> roomService.delete(roomId));
        verify(roomRepository, never()).deleteById(roomId);
    }
    
    @Test
    public void testDeleteRoomWithoutCleaningSessions() {
        Long roomId = 110L;
        
        // Create a room object with no cleaning sessions
        Room room = new Room("e252");
        room.setId(roomId);
        room.setCleaningSessions(new ArrayList<>()); // Ensure room has no cleaning sessions
        
        // Stub the behavior of findById to return an Optional containing the room
        when(roomRepository.findById(roomId)).thenReturn(Optional.of(room));

        // Call the delete method of the room service
        roomService.delete(roomId);
        
        // Verify that deleteById method of roomRepository is called with correct Id
        verify(roomRepository).deleteById(roomId);
        
        // Verify that findById method of roomRepository is called exactly once with correct Id
        verify(roomRepository, times(1)).findById(roomId);
        
        // Verify that there are no more interactions with roomRepository
        verifyNoMoreInteractions(roomRepository);
    }
}

