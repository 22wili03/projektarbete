package hig.domain;

import jakarta.persistence.*;
import java.time.LocalDateTime;


@Entity
public class Payday {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "`dateOfDeadline`")
    private LocalDateTime dateOfDeadline;
    
    @Column(name = "`dateOfPayday`")
    private LocalDateTime dateOfPayday;
    
    public Payday(LocalDateTime dateOfPayday, LocalDateTime dateOfDeadline) {
        this.dateOfDeadline = dateOfDeadline;
        this.dateOfPayday = dateOfPayday;
    }
    
    public Payday() {};

    public LocalDateTime getDateOfPayday() {
        return dateOfPayday;
    }

    public void setDateOfPayday(LocalDateTime dateOfPayday) {
        if(dateOfPayday == null) {
            throw new NullPointerException("Date and Time cannot be null");
        }
        this.dateOfPayday = dateOfPayday;    
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateOfDeadline() {
        return dateOfDeadline;
    }

    public void setDateOfDeadline(LocalDateTime dateOfDeadline) {
        if(dateOfDeadline == null) {
            throw new NullPointerException("Date and Time cannot be null");
        }
        this.dateOfDeadline = dateOfDeadline;
    }
}
