package hig.domain;

import jakarta.persistence.*;
import java.time.LocalDateTime;


@Entity
public class Cleaning {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "`when`")
    private LocalDateTime when = LocalDateTime.now();
    
    @ManyToOne
    @JoinColumn(name = "where_id")
    private Room where;
    
    @ManyToOne
    @JoinColumn(name = "who_id")
    private Person who;

    protected Cleaning() {
    }

    public Cleaning(Person who, Room where) {
        this.where = where;
        this.who = who;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getWhen() {
        return when;
    }

    public void setWhen(LocalDateTime when) {
        if(when == null) {
            throw new NullPointerException("Date and Time cannot be null");
        }
        this.when = when;
    }

    public Room getWhere() {
        return where;
    }

    public void setWhere(Room where) {
        if(where == null) {
            throw new NullPointerException("The value of Room cannot be null");
        }
        this.where = where;
    }

    public Person getWho() {
        return who;
    }

    public void setWho(Person who) {
        if(who == null) {
            throw new NullPointerException("The value of Person cannot be null");
        }
        this.who = who;
    }
}
