package hig.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.LocalDateTime;


@Entity
@Table(name = "payment")
public class Payment {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;
    
    @Column(name = "date")
    private LocalDateTime time;
    
    @Column(name = "amount")
    private Double amount;

    public Payment(Person person, LocalDateTime time, Double amount) {
        setPerson(person);
        setTime(time);
        setAmount(amount);
    }
    
    public Payment() {}
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        if(person == null) {
            throw new NullPointerException("PersonId cannot be null or less than 1");
        }
        this.person = person;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        if(time == null) {
            throw new NullPointerException("Date and Time cannot be null");
        }
        this.time = time;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        if(amount < 1 || amount == null) {
            throw new NullPointerException("Amount cannot be null or less than zero");
        }
        this.amount = amount;
    }

}
