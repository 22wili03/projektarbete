package hig.domain;

import com.sun.istack.NotNull;
import jakarta.persistence.*;
import java.util.List;


@Entity
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    private String name;
    
    @OneToMany(mappedBy="where", cascade = CascadeType.ALL)
    private List<Cleaning> cleaningSessions;
    
    protected Room() {}

    public Room(String name) {
        setName(name);
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.trim().isEmpty()) 
            throw new IllegalArgumentException("Name cannot be null or empty");
        this.name = name;
    }
    
    public List<Cleaning> getCleaningSessions() {
        return cleaningSessions;
    }

    public void setCleaningSessions(List<Cleaning> cleaningSessions) {
        this.cleaningSessions = cleaningSessions;
    }
}
