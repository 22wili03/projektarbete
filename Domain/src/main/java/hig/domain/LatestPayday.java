package hig.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import java.time.LocalDateTime;


@Entity
public class LatestPayday {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @OneToOne
    @JoinColumn(name = "person_id")
    private Person person;
    
    private LocalDateTime dateOfLatestPayday;
    
    public LatestPayday(Person person, LocalDateTime dateOfLatestPayday) {
        this.person = person;
        this.dateOfLatestPayday = dateOfLatestPayday;
    }
    
    public LatestPayday() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDateTime getDateOfLatestPayday() {
        return dateOfLatestPayday;
    }

    public void setDateOfLatestPayday(LocalDateTime dateOfLatestPayday) {
        this.dateOfLatestPayday = dateOfLatestPayday;
    }
    
    
}
