package hig.domain;

import com.sun.istack.NotNull;
import jakarta.persistence.*;
import java.util.UUID;


@Entity
@Table(name = "person")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "first_name")
    private String firstName;
    
    @Column(name = "last_name")
    private String lastName;
    
    @Column(name = "age")
    private Integer age;
    
    
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private RoleType role;
    
    @Column(unique = true)
    private String tag = UUID.randomUUID().toString();
    

    public Person(@NotNull String firstName, @NotNull String lastName, Integer age, RoleType role) {
        setFirstName(firstName);
        setLastName(lastName);
        setAge(age);
        setRole(role);
    }
    
    public Person() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("First name cannot be null or empty");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    
    @NotNull
    public void setLastName(String lastName) {
        if (lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("Last name cannot be null or empty");
        }
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        if (age == null)  // Throws an exception if age value is null
            throw new IllegalArgumentException("Age cannot be null");
        else if (age < 0)  // Throws an exception if age value is negative
            throw new IllegalArgumentException("Age cannot be negative");
        this.age = age;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    
    public void setRole(RoleType role) {
        if(role != RoleType.ROLE_ADMIN)
            role = RoleType.ROLE_USER;
        this.role = role;
    }
    
    public RoleType getRole() {
        return this.role;
    }
}
