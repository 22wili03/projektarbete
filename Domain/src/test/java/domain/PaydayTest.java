package hig.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class PaydayTest {

    private Payday payday;
    private LocalDateTime dateOfPayday;
    private LocalDateTime dateOfDeadline;

    @BeforeEach
    void setUp() {
        dateOfPayday = LocalDateTime.of(2023, 5, 28, 12, 0);
        dateOfDeadline = LocalDateTime.of(2023, 5, 27, 12, 0);
        payday = new Payday(dateOfPayday, dateOfDeadline);
    }

    @Test
    void testConstructor() {
        assertNotNull(payday);
        assertEquals(dateOfPayday, payday.getDateOfPayday());
        assertEquals(dateOfDeadline, payday.getDateOfDeadline());
    }

    @Test
    void testGetDateOfPayday() {
        assertEquals(dateOfPayday, payday.getDateOfPayday());
    }

    @Test
    void testSetDateOfPayday() {
        LocalDateTime newDateOfPayday = LocalDateTime.of(2023, 6, 28, 12, 0);
        payday.setDateOfPayday(newDateOfPayday);
        assertEquals(newDateOfPayday, payday.getDateOfPayday());

        Exception exception = assertThrows(NullPointerException.class, () -> {
            payday.setDateOfPayday(null);
        });
        assertEquals("Date and Time cannot be null", exception.getMessage());
    }

    @Test
    void testGetId() {
        payday.setId(1L);
        assertEquals(1L, payday.getId());
    }

    @Test
    void testSetId() {
        payday.setId(2L);
        assertEquals(2L, payday.getId());
    }

    @Test
    void testGetDateOfDeadline() {
        assertEquals(dateOfDeadline, payday.getDateOfDeadline());
    }

    @Test
    void testSetDateOfDeadline() {
        LocalDateTime newDateOfDeadline = LocalDateTime.of(2023, 6, 27, 12, 0);
        payday.setDateOfDeadline(newDateOfDeadline);
        assertEquals(newDateOfDeadline, payday.getDateOfDeadline());

        Exception exception = assertThrows(NullPointerException.class, () -> {
            payday.setDateOfDeadline(null);
        });
        assertEquals("Date and Time cannot be null", exception.getMessage());
    }
}
