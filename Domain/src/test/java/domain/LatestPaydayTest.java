package hig.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class LatestPaydayTest {

    private LatestPayday latestPayday;
    private Person person;
    private LocalDateTime dateOfLatestPayday;

    @BeforeEach
    void setUp() {
        person = new Person();
        person.setFirstName("John");
        person.setLastName("Doe");
        person.setAge(30);
        person.setRole(RoleType.ROLE_USER);
        dateOfLatestPayday = LocalDateTime.of(2023, 5, 28, 12, 0);
        latestPayday = new LatestPayday(person, dateOfLatestPayday);
    }

    @Test
    void testConstructor() {
        assertNotNull(latestPayday);
        assertEquals(person, latestPayday.getPerson());
        assertEquals(dateOfLatestPayday, latestPayday.getDateOfLatestPayday());
    }

    @Test
    void testGetId() {
        latestPayday.setId(1L);
        assertEquals(1L, latestPayday.getId());
    }

    @Test
    void testSetId() {
        latestPayday.setId(2L);
        assertEquals(2L, latestPayday.getId());
    }

    @Test
    void testGetPerson() {
        assertEquals(person, latestPayday.getPerson());
    }

    @Test
    void testSetPerson() {
        Person newPerson = new Person();
        newPerson.setFirstName("Jane");
        newPerson.setLastName("Smith");
        newPerson.setAge(25);
        newPerson.setRole(RoleType.ROLE_ADMIN);
        latestPayday.setPerson(newPerson);
        assertEquals(newPerson, latestPayday.getPerson());
    }

    @Test
    void testGetDateOfLatestPayday() {
        assertEquals(dateOfLatestPayday, latestPayday.getDateOfLatestPayday());
    }

    @Test
    void testSetDateOfLatestPayday() {
        LocalDateTime newDateOfLatestPayday = LocalDateTime.of(2023, 6, 28, 12, 0);
        latestPayday.setDateOfLatestPayday(newDateOfLatestPayday);
        assertEquals(newDateOfLatestPayday, latestPayday.getDateOfLatestPayday());
    }
}
