package domain;

import hig.domain.Person;
import hig.domain.RoleType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class PersonTest {
    
    public PersonTest() {}

    private Person person;

    @BeforeEach
    public void setUp() {
        person = new Person("Nisse", "Hult", 25, RoleType.ROLE_USER);
    }

    @Test
    public void testConstructor() {
        assertNotNull(person);
        assertEquals("Nisse", person.getFirstName());
        assertEquals("Hult", person.getLastName());
        assertEquals(Integer.valueOf(25), person.getAge());
    }
    
    @Test
    public void testSetIdAndGetId() {
        person.setId(1L);
        assertEquals(Long.valueOf(1), person.getId());
    }

    @Test
    public void testGetFirstNameAndSetFirstName() {
        person.setFirstName("Kalle");
        assertEquals("Kalle", person.getFirstName());
    }

    @Test
    public void testGetLastNameAndSetLastName() {
        person.setLastName("Anka");
        assertEquals("Anka", person.getLastName());
    }

    @Test
    public void testGetAgeAndSetAge() {
        person.setAge(30);
        assertEquals(Integer.valueOf(30), person.getAge());
    }

    @Test
    public void testGetTagAndSetTag() {
        person.setTag("abc123");
        assertEquals("abc123", person.getTag());
    }
    
    @Test
    public void testSetAndGetRole() {
        person.setRole(RoleType.ROLE_ADMIN);
        assertEquals(RoleType.ROLE_ADMIN, person.getRole());
        
        person.setRole(RoleType.ROLE_USER);
        assertEquals(RoleType.ROLE_USER, person.getRole());
    }
    
}
