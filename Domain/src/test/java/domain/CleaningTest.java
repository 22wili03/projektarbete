package domain;

import hig.domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CleaningTest {
    private Person person1;
    private Person person2;
    private Room room1; 
    private Room room2; 
    private Cleaning cleaning;
    
    
    public CleaningTest() {}
    
    @BeforeEach
    public void setUp() {
        person1 = new Person("Alice", "Lundholm", 21, RoleType.ROLE_USER);
        person2 = new Person("Omar", "Rajoub", 21, RoleType.ROLE_ADMIN);
        room1 = new Room("e255");
        room2 = new Room("e257");
        cleaning = new Cleaning(person1, room1);
    }
    
    @Test
    public void testSetWhere() {
        assertEquals(cleaning.getWhere(), room1);
        cleaning.setWhere(room2);
        assertEquals(cleaning.getWhere(), room2);
    }
    
    @Test
    public void testSetWhereToNull() {
        assertThrows(Exception.class, () -> cleaning.setWhere(new Room(null)));
    }
    
    @Test
    public void testSetWho() {
        assertEquals(cleaning.getWho(), person1);
        cleaning.setWho(person2);
        assertEquals(cleaning.getWho(), person2);
    }
    
    @Test
    public void testSetWhoToNull() {
        assertThrows(Exception.class, () -> cleaning.setWho(null));
    }
}
