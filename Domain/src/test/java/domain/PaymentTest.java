package domain;

import hig.domain.*;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class PaymentTest {
    private Person person1;
    private Person person2;
    private LocalDateTime time1 = LocalDateTime.now();
    private LocalDateTime time2 = LocalDateTime.now().plusDays(5);

    private Double amount1; 
    private Double amount2; 
    private Payment payment;
    
    public PaymentTest() {
    }
    
    @BeforeEach
    public void setUp() {
        person1 = new Person("Alice", "Lundholm", 21, RoleType.ROLE_USER);
        person2 = new Person("Omar", "Rajoub", 21, RoleType.ROLE_ADMIN);
        amount1 = 125d;
        amount2 = 25d;
        payment = new Payment(person1, time1, amount1);
    }
    
    @Test
    public void testSetPerson() {
        assertEquals(payment.getPerson().getFirstName(), person1.getFirstName());
        payment.setPerson(person2);
        assertEquals(payment.getPerson().getFirstName(), person2.getFirstName());
    }
    
    @Test
    public void testSetPersonToNull() {
        assertThrows(Exception.class, () -> payment.setPerson(null));
    }
    
    @Test
    public void testSetTime() {
        assertEquals(payment.getTime(), time1);
        payment.setTime(time2);
        assertEquals(payment.getTime(), time2);
    }
    
    @Test
    public void testSetTimeToNull() {
        assertThrows(Exception.class, () -> payment.setTime(null));
    }
    
    @Test
    public void testSetAmount() {
        assertEquals(payment.getAmount(), amount1);
        payment.setAmount(amount2);
        assertEquals(payment.getAmount(), amount2);
    }
    
    @Test
    public void testSetAmountToNull() {
        assertThrows(Exception.class, () -> payment.setAmount(null));
    }

}
