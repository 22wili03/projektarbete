package domain;

import hig.domain.Room;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class RoomTest {
    Room room = new Room("e250");
    
    public RoomTest() {}

    @Test
    public void testSetName() {
        assertEquals(room.getName(), "e250");
        room.setName("e252");
        assertEquals(room.getName(), "e252");
    }
    
    @Test
    public void testSetNameToNull() {
        assertThrows(Exception.class, () -> room.setName(null));
    }
    
    @Test
    public void testSetNameToEmptyString() {
        assertThrows(Exception.class, () -> room.setName("     "));
    }

}
