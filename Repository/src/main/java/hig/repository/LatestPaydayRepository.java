package hig.repository;

import hig.domain.LatestPayday;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LatestPaydayRepository extends JpaRepository<LatestPayday, Long> {
    public Optional<LatestPayday> findByPersonId(long id);
}
