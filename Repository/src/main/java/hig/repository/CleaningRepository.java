package hig.repository;

import hig.domain.Cleaning;
import java.time.*;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CleaningRepository extends JpaRepository<Cleaning, Long>{

    //Finds all cleanings by room id that is yet to happen.
    public List<Cleaning> findByWhereIdAndWhenGreaterThan(Long roomId, LocalDateTime from);
    
    //Finds all cleanings for a person between two given dates.
    public List<Cleaning> findByWhoIdAndWhenGreaterThanAndWhenLessThan(Long id, LocalDateTime dateFrom, LocalDateTime dateTo);
}
