package hig.repository;

import hig.domain.Payment;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long>{

    // Custom query method to find payments with the given person ID and date earlier than the specified date
    public List<Payment> findByPersonIdAndTimeGreaterThan(Long personId, LocalDateTime time);
}
