package hig.repository;

import hig.domain.Payday;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaydayRepository extends JpaRepository<Payday, Long>{
    
    // Finds the paydays that has deadline after the given date.
    public List<Payday> findByDateOfDeadlineGreaterThan(LocalDateTime dateOfPayday);
    
    public Optional<Payday> findTopByOrderByIdDesc(); //Finds the last payday.
}
