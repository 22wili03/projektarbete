package hig.repository;

import hig.domain.Person;
import hig.domain.RoleType;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    public Optional<Person> findByTag(String personalTag);
    public Optional<Person> deleteByTag(String personalTag);
    public Optional<Person> existsByTag(String personalTag);
    
    public Integer countByRole(RoleType role);
}
