package hig.rest.test.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import hig.BasicApplication;
import hig.domain.Person;
import hig.domain.RoleType;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author willi
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BasicApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class RoomControllerIntegrationTest {
    
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private RoomRepository roomRepository;
    
    @Autowired
    private PersonRepository personRepository;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    private String adminTag = "";
    private String userTag = "";
    
    public RoomControllerIntegrationTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        roomRepository.deleteAll();
        personRepository.deleteAll();
        Person admin = new Person("William", "Lindahl", 21, RoleType.ROLE_ADMIN);
        Person user = new Person("Omar", "Rajoub", 21, RoleType.ROLE_USER);
        adminTag = admin.getTag();
        userTag = user.getTag();
        personRepository.save(admin);
        personRepository.save(user);
    }
    
    @AfterEach
    public void tearDown() {
        roomRepository.deleteAll();
        personRepository.deleteAll();
    }
    
    @Test
    public void testGetAll() throws Exception {
        roomRepository.save(new Room("99524"));
        roomRepository.save(new Room("99522"));
        
        assertEquals(2, roomRepository.count());
        
        MvcResult result = mockMvc.perform(get("/room")
                .header("Caller-Uuid", adminTag)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2))
                .andReturn();
        
    }
    
    @Test
    public void testCreate() throws Exception {
        Room room = new Room("99524");
        String roomJson = objectMapper.writeValueAsString(room);
        // Test creating person as an Admin, should work
        MvcResult result = mockMvc.perform(post("/room")
                .header("Caller-Uuid", adminTag)
                .contentType(MediaType.APPLICATION_JSON)
                .content(roomJson))
                .andExpect(status().isOk())
                .andReturn();
        
        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);  // Print the response content
        
        Room room2 = new Room("99524");
        String room2Json = objectMapper.writeValueAsString(room2);
        // Test creating person as an User, should not work
        result = mockMvc.perform(post("/room")
                .header("Caller-Uuid", userTag)
                .contentType(MediaType.APPLICATION_JSON)
                .content(room2Json))
                .andExpect(status().isForbidden())
                .andReturn();
        
        responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);  // Print the response content
    }
    
    @Test
    public void testDelete() throws Exception {
        Room room = new Room("99524");
        room = roomRepository.save(room);
        
        assertEquals(1, roomRepository.count());
        
        // Test creating person as an Admin, should work
        MvcResult result = mockMvc.perform(delete("/room/delete/" + room.getId())
                .header("Caller-Uuid", adminTag)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        
        assertEquals(0, roomRepository.count());
        
        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);  // Print the response content
        
        
    }
    
}
