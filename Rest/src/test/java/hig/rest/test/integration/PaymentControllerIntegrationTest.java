package hig.rest.test.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import hig.BasicApplication;
import hig.domain.LatestPayday;
import hig.domain.Payday;
import hig.domain.Person;
import hig.domain.RoleType;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.LatestPaydayRepository;
import hig.repository.PaydayRepository;
import hig.repository.PaymentRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import java.time.LocalDateTime;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author willi
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BasicApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class PaymentControllerIntegrationTest {
    
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private PaymentRepository paymentRepository;
    
    @Autowired
    private PersonRepository personRepository;
    
    @Autowired
    private LatestPaydayRepository latestPaydayRepository;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    private Person admin;
    private Person user;
    
    public PaymentControllerIntegrationTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        paymentRepository.deleteAll();
        latestPaydayRepository.deleteAll();
        personRepository.deleteAll();
        admin = new Person("William", "Lindahl", 21, RoleType.ROLE_ADMIN);
        user = new Person("Omar", "Rajoub", 21, RoleType.ROLE_USER);
        personRepository.save(admin);
        personRepository.save(user);
        admin = personRepository.findByTag(admin.getTag()).get();
        user = personRepository.findByTag(user.getTag()).get();
    }
    
    @AfterEach
    public void tearDown() {
        paymentRepository.deleteAll();
        latestPaydayRepository.deleteAll();
        personRepository.deleteAll();
        
    }
    
    @Test
    public void testCreate() throws Exception {
        Room room = new Room("99524");
        String roomJson = objectMapper.writeValueAsString(room);
        LocalDateTime date = LocalDateTime.now().plusDays(1);
        double amount = 152.5;
        // Test creating person as an Admin, should work
        MvcResult result = mockMvc.perform(post("/payment/create/" + amount)
                .param("date", date.toString())
                .header("Caller-Uuid", admin.getTag())
                .header("Target-Uuid", user.getTag())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        
        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);  // Print the response content
        
        latestPaydayRepository.save(new LatestPayday(user, LocalDateTime.now()));
        System.out.println(latestPaydayRepository.findByPersonId(user.getId()).get().getDateOfLatestPayday());
        
        date = LocalDateTime.now().minusDays(1);
        amount = 152.5;
        // Test creating person a payment with ealier date than latest payday, should no work.
        result = mockMvc.perform(post("/payment/create/" + amount)
                .param("date", date.toString())
                .header("Caller-Uuid", admin.getTag())
                .header("Target-Uuid", user.getTag())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        
        responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);  // Print the response content
        
        
    }
    
}
