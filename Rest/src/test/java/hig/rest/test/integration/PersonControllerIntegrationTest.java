
package hig.rest.test.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import hig.BasicApplication;
import hig.domain.Person;
import hig.domain.RoleType;
import hig.repository.PersonRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 *
 * @author willi
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BasicApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class PersonControllerIntegrationTest {
    
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private PersonRepository personRepository;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    private String adminTag = "";
    private String userTag = "";
    
    public PersonControllerIntegrationTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() throws Exception {
        personRepository.deleteAll();
        // Create a new Person object
        Person admin = new Person("William", "Lindahl", 21, RoleType.ROLE_ADMIN);
        Person user = new Person("Omar", "Rajoub", 21, RoleType.ROLE_USER);
        adminTag = admin.getTag();
        userTag = user.getTag();
        personRepository.save(admin);
        personRepository.save(user); 
        
    }
    
    @AfterEach
    public void tearDown() {
        personRepository.deleteAll();
    }
    
    
    @Test
    void testFindAll() throws Exception {

        // Perform the GET request to the /person endpoint
        MvcResult result = mockMvc.perform(get("/person")
                .header("Caller-Uuid", userTag)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);  // Print the response content
    }
    
    @Test
    void testCreate() throws Exception {
        Person person = new Person("Sven", "Svensson", 45, RoleType.ROLE_USER);
        Person person2 = new Person("bert", "Anderssons", 35, RoleType.ROLE_USER);
        String personJson = objectMapper.writeValueAsString(person);
        String person2Json = objectMapper.writeValueAsString(person2);
        // Test creating person as an Admin, should work
        MvcResult result = mockMvc.perform(post("/person")
                .header("Caller-Uuid", adminTag)
                .contentType(MediaType.APPLICATION_JSON)
                .content(personJson))
                .andExpect(status().isOk())
                .andReturn();
        
        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);  // Print the response content
        
        // Test creating person as a User, should not work because missing permission
        result = mockMvc.perform(post("/person")
                .header("Caller-Uuid", userTag)
                .contentType(MediaType.APPLICATION_JSON)
                .content(person2Json))
                .andExpect(status().isForbidden())
                .andReturn();
        
        responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);  // Print the response content
      
    }
    
    @Test
    void testDeleteByTag() throws Exception {
        
        assertEquals(2, personRepository.count());
        
        MvcResult result = mockMvc.perform(delete("/person/delete")
                .header("Caller-Uuid", adminTag)
                .header("Target-Uuid", userTag)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        
        assertEquals(1, personRepository.count());
        
        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);  // Print the response content
    }

}
