package hig.rest.test.integration;

import hig.domain.Payday;
import hig.rest.PaydayController;
import hig.service.payday.PaydayService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(PaydayController.class)
public class PaydayControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PaydayService paydayService;

    private List<Long> personIdList;
    private Payday payday;

    @BeforeEach
    void setUp() {
        personIdList = Arrays.asList(1L, 2L, 3L);
        payday = new Payday();
        payday.setDateOfDeadline(LocalDateTime.now());

        when(paydayService.create(any(List.class), any(LocalDateTime.class))).thenReturn(payday);
        when(paydayService.create(any(List.class))).thenReturn(payday);
    }

    @Test
    void testCreatePaydayWithDeadline() throws Exception {
        String dateOfDeadline = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);

        mockMvc.perform(post("/payday/create/date")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[1, 2, 3]")
                .param("dateOfDeadline", dateOfDeadline))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dateOfDeadline").isNotEmpty());
    }

    @Test
    void testCreatePaydayWithoutDeadline() throws Exception {
        mockMvc.perform(post("/payday/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[1, 2, 3]"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dateOfDeadline").isNotEmpty());
    }
}
