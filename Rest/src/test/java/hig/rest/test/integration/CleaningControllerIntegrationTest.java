package hig.rest.test.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import hig.BasicApplication;
import hig.domain.Person;
import hig.domain.RoleType;
import hig.domain.Room;
import hig.repository.RoomRepository;
import hig.repository.CleaningRepository;

import hig.repository.PersonRepository;
import hig.service.cleaning.CleaningDTO;
import java.time.LocalDateTime;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BasicApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class CleaningControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private RoomRepository roomRepository;
    
    @Autowired
    private CleaningRepository cleaningRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private String adminTag = "";
    private String userTag = "";

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() throws Exception {
        cleaningRepository.deleteAll();
        personRepository.deleteAll();
        // Create a new Person object
        Person admin = new Person("William", "Lindahl", 21, RoleType.ROLE_ADMIN);
        Person user = new Person("Omar", "Rajoub", 21, RoleType.ROLE_USER);
        adminTag = admin.getTag();
        userTag = user.getTag();
        personRepository.save(admin);
        personRepository.save(user);
        
        Room room1 = new Room("Rum ett");
        Room room2 = new Room("Rum två");
        roomRepository.save(room1);
        roomRepository.save(room2);


        
    }

    @AfterEach
    public void tearDown() {
        cleaningRepository.deleteAll();
        personRepository.deleteAll();
    }

    @Test
    void testCreateCleaning() throws Exception {
        long roomId = roomRepository.findAll().get(0).getId();
        // Test creating a cleaning with valid roomId
        MvcResult result = mockMvc.perform(post("/cleaning/create/{roomId}", roomId)
                .header("Caller-Uuid", userTag)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);

        // Test creating a cleaning with invalid roomId
        result = mockMvc.perform(post("/cleaning/create/{roomId}", -1)
                .header("Caller-Uuid", userTag)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Id cannot be less than 1")))
                .andReturn();

        responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);
    }

    @Test
    void testGetByRoomIdAndDateTime() throws Exception {
        long roomId = 1;
        LocalDateTime dateTime = LocalDateTime.now();

        MvcResult result = mockMvc.perform(get("/cleaning/{roomId}/when", roomId)
                .header("Caller-Uuid", userTag)
                .param("dateTime", dateTime.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);
    }

    @Test
    void testCheckMyCleanings() throws Exception {
        LocalDateTime dateFrom = LocalDateTime.now().minusDays(1);
        LocalDateTime dateTo = LocalDateTime.now().plusDays(1);

        MvcResult result = mockMvc.perform(get("/cleaning/checkMyCleanings")
                .header("Caller-Uuid", userTag)
                .param("dateFrom", dateFrom.toString())
                .param("dateTo", dateTo.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);
    }

    @Test
    void testGetByRoomIdForCurrentDay() throws Exception {
        long roomId = 1;

        MvcResult result = mockMvc.perform(get("/cleaning/{roomId}", roomId)
                .header("Caller-Uuid", userTag)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String responseContent = result.getResponse().getContentAsString();
        System.out.println("Response content: " + responseContent);
    }
}
