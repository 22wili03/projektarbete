package hig.aspect;

import hig.domain.RoleType;
import hig.repository.PersonRepository;
import jakarta.servlet.http.HttpServletRequest;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class SecurityAspect {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private PersonRepository personRepository;
    
    @Before("execution(* hig.rest..*Controller.*(..)) && !execution(* hig.rest.PersonController.create(..))")
    public void checkAuthentication() {
        Optional.ofNullable(request.getHeader("Caller-Uuid"))
                .filter(s -> personRepository.findByTag(s).isPresent())
                .orElseThrow(() -> new NoSuchElementException("User does not exist"));
    }
    
    public void checkAuthorization() {
        Optional.ofNullable(request.getHeader("Caller-Uuid"))
                .filter(s -> personRepository.findByTag(s).get().getRole().equals(RoleType.ROLE_ADMIN))
                .orElseThrow(() -> new SecurityException("Permission denied"));
    }
    
    @Before("@annotation(hig.annotations.AdminAccess)")
    public void checkIfAdmin() {
        checkAuthorization();
    }
    
    @Before("execution(* hig.rest.PersonController.create(..)))")
    public void checkForAdmins() {
        if(personRepository.countByRole(RoleType.ROLE_ADMIN) > 0) {
            checkIfAdmin();
        }
    }
}
