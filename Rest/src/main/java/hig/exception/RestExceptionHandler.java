package hig.exception;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {SecurityException.class})
    public ResponseEntity<ExceptionWrapper> handleSecurityException(SecurityException ex) {
        return new ResponseEntity<>(new ExceptionWrapper(ex), HttpStatus.FORBIDDEN);
    }

    public static class ExceptionWrapper {

        private final String type;
        private final String message;

        public ExceptionWrapper(Exception ex) {
            this.type = ex.getClass().getSimpleName();
            this.message = ex.getMessage();
        }

        public String getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }
    }
    
    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<ExceptionWrapper> handleIllegalArgumentException(IllegalArgumentException e) {
        return new ResponseEntity<>(new ExceptionWrapper(e), HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<ExceptionWrapper> handleGenericException(Exception ex) {
        return new ResponseEntity<>(new ExceptionWrapper(ex), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
