package hig.exception;

public class RemoveLastAdminNotAllowException extends RuntimeException {
    public RemoveLastAdminNotAllowException(String message) {
        super(message);
    }
}
