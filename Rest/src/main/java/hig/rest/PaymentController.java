package hig.rest;

import hig.annotations.AdminAccess;
import hig.domain.Payment;
import hig.service.payment.PaymentService;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("payment")
public class PaymentController {
    private final PaymentService service;

    @Autowired
    public PaymentController(PaymentService service) {
        this.service = service; 
    }
    
    @AdminAccess
    @PostMapping("create/{amount}")
    public Payment create(@RequestHeader("Target-Uuid") String personalTag, @RequestParam("date")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime date, @PathVariable Double amount) {
        return service.create(personalTag, date, amount);
    }
}