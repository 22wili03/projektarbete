package hig.rest;

import hig.annotations.AdminAccess;
import hig.domain.Room;
import hig.repository.RoomRepository;
import hig.service.room.*;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.MissingRequiredPropertiesException;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("room")
public class RoomController {

    private final RoomService service;
    private final RoomMapper mapper;
    
    @Autowired
    RoomRepository roomRepository;

    public RoomController(
            RoomService service, 
            RoomMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    @AdminAccess
    public RoomDTO create(@RequestBody Room room) {
        if(room.getName() == null)
            throw new MissingRequiredPropertiesException();
        return mapper.toDto(service.create(room));
    }
    
    @GetMapping
    public List<RoomDTO> findAll() {
        return service.findAll();
    }
    
    @DeleteMapping("delete/{id}")
    @AdminAccess
    public void delete(@PathVariable Long id) {
        if(!roomRepository.existsById(id))
            throw new NoSuchElementException("Room doesnt exist!");
        service.delete(id);
    }
}
