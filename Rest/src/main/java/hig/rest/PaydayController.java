package hig.rest;

import hig.annotations.AdminAccess;
import hig.domain.Payday;
import hig.service.payday.PaydayService;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("payday")
public class PaydayController {
    private final PaydayService service;

    @Autowired
    public PaydayController(PaydayService service) {
        this.service = service;
    }
    
    @AdminAccess
    @PostMapping("create/date")
    public Payday create(@RequestBody List<Long> personIdList, @RequestParam("dateOfDeadline")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateOfDeadline) {
        return service.create(personIdList, dateOfDeadline);
    }
    
    @AdminAccess
    @PostMapping("create")
    public Payday create(@RequestBody List<Long> personIdList) {
        return service.create(personIdList);
    }
    
}
