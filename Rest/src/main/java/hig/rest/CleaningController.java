package hig.rest;

import hig.annotations.AdminAccess;
import hig.service.cleaning.*;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("cleaning")
public class CleaningController {

    private final CleaningService service;

    @Autowired
    public CleaningController(CleaningService service) {
        this.service = service;
    }

    @PostMapping("create/{roomId}")
    public CleaningDTO create(@RequestHeader("Caller-Uuid") String personalTag, @PathVariable Long roomId) {
        if(roomId <= 0) {
            throw new IllegalArgumentException("Id cannot be less than 1");
        }
        return service.create(roomId, personalTag);
    }
    
    @GetMapping("{roomId}/when")
    public List<CleaningDTO> getByRoomIdAndDateTime(@PathVariable Long roomId, 
            @RequestParam("dateTime")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateTime) {
        return service.getByRoomIdAndDateTime(roomId, dateTime);
    }
    
    @GetMapping("checkMyCleanings")
    public List<CleaningDTO> getByPersonIdAndDateTimeInterval(@RequestHeader("Caller-Uuid") String personalTag, 
            @RequestParam("dateFrom")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateFrom, 
            @RequestParam("dateTo")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateTo) {
        
        return service.getByPersonIdAndDateTimeInterval(service.TagToId(personalTag), dateFrom, dateTo);
    }
    
    @AdminAccess
    @GetMapping("getCleanings/{personId}")
    public List<CleaningDTO> getByPersonIdAndDateTimeInterval(@PathVariable Long personId, 
            @RequestParam("dateFrom")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateFrom, 
            @RequestParam("dateTo")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateTo) {
        
        return service.getByPersonIdAndDateTimeInterval(personId, dateFrom, dateTo);
    }
    
    @GetMapping("{roomId}")
    public List<CleaningDTO> getByRoomIdForCurrentDay(@PathVariable Long roomId) {
        return service.getByRoomIdForCurrentDay(roomId);
    } 
    
}
