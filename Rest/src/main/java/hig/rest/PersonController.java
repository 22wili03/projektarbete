package hig.rest;

import hig.annotations.AdminAccess;
import hig.service.person.PersonService;
import hig.domain.Person;
import hig.domain.RoleType;
import hig.exception.RemoveLastAdminNotAllowException;
import hig.repository.PersonRepository;
import hig.service.person.*;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.MissingRequiredPropertiesException;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("person")
public class PersonController {

    private final PersonService service;

    @Autowired
    PersonRepository personRepository;
    
    @Autowired
    public PersonController(PersonService service) {
        this.service = service;
    }

    @GetMapping
    public List<PersonSimpleDTO> findAll() {
        return service.getAllPeople();
    }

    @GetMapping("{id}")
    public PersonSimpleDTO findOne(@PathVariable Long id) {
        return service.getPerson(id);
    }

    @PostMapping()
    public PersonSimpleDTO create(@RequestBody Person person) {
        if(person.getFirstName() == null || person.getLastName() == null)
            throw new MissingRequiredPropertiesException();
        if(personRepository.countByRole(RoleType.ROLE_ADMIN) == 0) 
            person.setRole(RoleType.ROLE_ADMIN);
        return service.create(person);
    }
    
    @DeleteMapping("delete")
    @AdminAccess
    public void deleteByTag(@RequestHeader("Target-Uuid") String tag) {
        if(!personRepository.existsByTag(tag).isPresent())
            throw new NoSuchElementException("User doesnt exist!");
        else if(personRepository.findByTag(tag).get().getRole() == RoleType.ROLE_ADMIN && personRepository.countByRole(RoleType.ROLE_ADMIN) <= 1)
            throw new RemoveLastAdminNotAllowException("Cannot delete the last admin");
        service.delete(personRepository.findByTag(tag).get());
    } 
}
